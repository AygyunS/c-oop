using System;

class ThrowingExceptions
{

    static void Main()
    {
        double number = double.Parse(Console.ReadLine());

        try
        {
            if (number < 0)
            {
                throw new ArgumentOutOfRangeException();
            }
            double squareRoot = Math.Sqrt(number);
            Console.WriteLine(squareRoot);
        }
        catch (FormatException fe)
        {
            Console.WriteLine("Invalid number!");
        }
        catch (ArgumentOutOfRangeException)
        {
            Console.WriteLine("Invalid number!");
        }
        finally
        {
            Console.WriteLine("Good bye!");
        }
    }
}

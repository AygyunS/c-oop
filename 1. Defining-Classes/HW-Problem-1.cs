//Problem 1.Persons
using System;

class Person
{
    private string name;
    private int age;
    public string email { get; set; }

    public Person()
    {
        this.name = "Pesho";
        this.age = 1;
    }
    public Person(string name, int age, string email)
    {
        this.Name = name;
        this.Age = age;
        this.email = email;
    }
    public Person(string name, int age)
       : this(name, age, null)
    {
        this.name = name;
        this.age = age;
    }


    public string Name
    {
        get { return this.name; }
        set { this.name = value; }
    }
    public int Age
    {
        get { return this.age; }
        set { this.age = value; }
    }
    public string Email
    {
        get { return this.email; }
        set
        {
            if(!string.IsNullOrEmpty(value) && !value.Contains("@"))
            {
                throw new ArgumentException("Email must contains @ character!");
            }
            this.email = email;
        }
    }
    

    public override string ToString()
    {
        return string.Format("Name:{0}, Age:{1}, Email:{2}", this.Name, this.Age,
            string.IsNullOrEmpty(this.Email) ? "not set" : this.Email);
    }
}
static void Main()
{
    Person firstPerson = new Person("Pesho", 21);
    Console.WriteLine(firstPerson);

}
//Problem 2.Laptop Shop

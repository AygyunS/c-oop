using System;

class Person
{
    private string firstName;
    private string lastName;
    private int age;
    public int building;

    public Person(string firstName, int building)
    {
        this.FirstName = firstName;
        this.Building = building;

    }
    public Person(string firstName, string lastName, int age, int building)
    {
        this.FirstName = firstName;
        this.LastName = lastName;
        this.Age = age;
        this.Building = building;

    }

    public string FirstName
    {
        get{ return this.firstName; }
        set
        {
            if(!string.IsNullOrEmpty(firstName))
            {
                throw new ArgumentNullException();
            }
            this.firstName = value;
        }
    }
    public string LastName
    {
        get { return this.lastName; }
        set
        {
            if(!string.IsNullOrEmpty(lastName))
            {
                throw new ArgumentNullException();
            }
            this.lastName = value;
        }
    }
    public int Age
    {
        get { return this.age; }
        set
        {
            if (value < 0)
            {
                throw new ArgumentException();
            }
            this.age = value;
        }
    }
    public int Building
    {
        get { return this.building; }
        set
        {
            if (value <= 0)
            {
                throw new ArgumentException();
            }
            this.building = value;
        }
    }

    public override string ToString()
    {
        return string.Format("Name:{0} {1}, Age:{2}, live on {3}", this.firstName, this.lastName, this.age, this.building);
    }



    static void Main(string[] args)
    {
        

        Person firstPerson = new Person("Aygyun", "Salim", 21, 91);
        Console.WriteLine(firstPerson);


    }
}
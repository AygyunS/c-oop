﻿using System;

class Person
{
    private string name;
    private int age;
    
    public Person()
    {
        this.name = "Pesho";
        this.age = 1;
    }
    public Person(string name, int age)
    {
        this.name = name;
        this.age = age;
    }

    public string Name
    {
        get { return this.name; }
        set { this.name = value; }
    }
    public int Age
    {
        get { return this.age; }
        set { this.age = value; }
    }
}
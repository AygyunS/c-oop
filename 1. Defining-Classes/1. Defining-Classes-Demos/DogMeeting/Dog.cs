using System;

class Dog
{
    private string name;
    private string breed;


    public Dog(string name = null, string breed = null)
    { 
        this.name = name;
        this.breed = breed; 
    }

    public string Name
    {
        get { return this.name; }
        set
        {
            if(string.IsNullOrEmpty(value))
            {
                throw new ArgumentNullException("Must enter name to dog!");
            }
            this.name = value;
        }
    }

    public string Breed
    {
        get { return this.breed; }
        set { this.breed = value; }
    }

    public void SayBau()
    {
        Console.WriteLine("{0} said: Bauuuuuu!", 
			this.name ?? "[unnamed dog]");
    }
    public override string ToString()
    {
        if (string.IsNullOrEmpty(this.name))
        {
            return "dog without name!";
        }
        else
            return "I am a dog with name: " + this.name;
    }

} 
